export default {
    'login': {
        index: 0,
        nextstate: 'gegevens',
        tab: 'inloggenTab',
        modelData: {
            email: "youri123",
            password: "jackie123",
        },
        completed: false,
    },
    'gegevens': {
        index: 1,
        nextstate: 'afrekenen',
        tab: 'gegevensTab',
        modelData: {
            firstname: "Pieter",
            preposition: "van",
            lastname: "douma",
            email: "pieter.douma@mail.com",
            phone: "06 123456",
            pickupaddress: "arnhemsestraat",
            pickuphousenumber: "6",
            pickupzipcode: "1234 AB",
            pickupcity: "Arnhem",
            pickupprovince: "Gelderland",
            dropoffaddress: "officierenstraat",
            dropoffhousenumber: "12",
            dropoffzipcode: "0987 ZX",
            dropoffcity: "Lobith",
            dropoffprovince: "Gelderland",
            sendlocation: "Keuken",
            item: "gasfornuis",
            width: "20",
            height: "10",
            length: "5",
            discountCode: "ai56kb",
            datestart: "20-06-2017",
            timestart: "13:13:13",
            dateend: "20-06-2018",
            timeend: "13:13:13",
        },
        completed: false,
    },
}

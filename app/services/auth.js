// endpoints
const HOST = 'http://codefest.dev/2017-back/public/api/'; //http://auth.bigparcel.nl/';
const LOGIN_URI = HOST + 'auth/login';
const REGISTER_URI = HOST + 'auth/register';
const USERINFO_URI = HOST + 'auth/userinfo';

const IDENTITY_TOKEN_PROPERTY = 'id_token';
const IDENTITY_USERNAME_PROPERTY = 'id_username';
// TODO: https://semantic-ui.com/behaviors/api.html
export default {
    // identity object is how we check auth status
    identity: {
        authenticated: false
    },

    parseJWT(body) { //$('body').append(body);
        return body;
    },

    login(context, data, callback, errorHandler) {
        let self = this;

        // console.log(Array.from(data.entries()));
        context.$http.post(LOGIN_URI, data).then(
            (response) => {
                if (!response.hasOwnProperty('body')) {
                    throw Error('Expected object with property "body".');
                }
                let body = response.body;
                localStorage.setItem(IDENTITY_USERNAME_PROPERTY, body.username);
                self.setIdentity(body.identity);
                self.identity.authenticated = true;

                // callback
                if (callback) {
                    callback(self);
                }
            },
            (error) => {
                let body = error.body;
                context.error = body.hasOwnProperty('error') ? body.error : body;
                errorHandler(error);
            }
        );
    },

    signup(context, data, callback, errorHandler){
        let self = this;

        console.log(Array.from(data.entries()));
        context.$http.post(REGISTER_URI, data).then(
            (response) => {
                if (!response.hasOwnProperty('body')) {
                    throw Error('Expected object with property "body".');
                }
                let bodyObject = self.parseJWT(response.body);
                localStorage.setItem(IDENTITY_USERNAME_PROPERTY, bodyObject.username);
                self.setIdentity(response[IDENTITY_TOKEN_PROPERTY]);
                self.identity.authenticated = true;

                // callback
                if (callback) {
                    callback(self);
                }
            },
            (err) => {
                let body = err.body;
                context.error = body.hasOwnProperty('error') ? body.error : body;
                errorHandler(err);
            }
        );
    },

    logout(){
        localStorage.removeItem(IDENTITY_TOKEN_PROPERTY);
        this.identity.authenticated = false
    },

    checkAuth(){
        var jwt = localStorage.getItem(IDENTITY_TOKEN_PROPERTY);
        // TODO: check JWT with backend server for expiration
        if (jwt) {
            this.identity.authenticated = true
        } else {
            this.identity.authenticated = false
        }
    },

    setIdentity(identity){
        return localStorage.setItem(IDENTITY_TOKEN_PROPERTY, identity);
    },

    getIdentity(){
        if (!this.identity.authenticated) {
            throw new Error('Not authenticated');
        }
        return localStorage.getItem(IDENTITY_TOKEN_PROPERTY);
    },

    getUsername(){
        if (!this.identity.authenticated) {
            throw new Error('Not authenticated');
        }
        return localStorage.getItem(IDENTITY_USERNAME_PROPERTY)
    },

    getUserInfo(context, callback){
        let self = this;

        // console.log(Array.from(data.entries()));
        context.$http.get(USERINFO_URI).then(
            (response) => {
                if (!response.hasOwnProperty('body')) {
                    throw Error('Expected object with property "body".');
                }
                let bodyObject = self.parseJWT(response.body);
                console.log(bodyObject);
                self.setIdentity(bodyObject.identity);

                // callback
                if (callback) {
                    callback(self);
                }
            },
            (err) => {
                context.error = err
            }
        )
    }
}

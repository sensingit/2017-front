

export default {
    showErrors(form, errorMsgs) {
        let $errorContainer = $(form).find('.error.message');
        let $errorList = $errorContainer.children().first();
        for (let name in errorMsgs) {
            let errorMsg = errorMsgs[name];
            let $errorNode = $('<li><b>' + name + '</b><br>' + errorMsg + '</li>');
            $errorList.append($errorNode);
        }
        $errorContainer.css('display', 'block');
    },

    hideErrors(form) {
        let $errorContainer = $(form).find('.error.message');
        $errorContainer.css('display', 'none');
    },
};

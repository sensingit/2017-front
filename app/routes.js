

export default [
    {
        path: '/',
        name: 'home',
        component: require('./components/home/home.vue'),
    },
    {
        path: '/login',
        name: 'login',
        component: require('./components/auth/login.vue'),
    },
    {
        path: '/logout',
        name: 'logout',
        component: require('./components/auth/logout.vue'),
    },
    {
        path: '/register',
        name: 'register',
        component: require('./components/auth/register.vue'),
        meta: {
            auth: false,
        },
    },
];

var gulp = require('gulp'),
    semantic = {
        watch: require('./assets/semantic/tasks/watch'),
        build: require('./assets/semantic/tasks/build')
    };
// import task with a custom task name
gulp.task('watch-semantic', semantic.watch);
gulp.task('build-semantic', semantic.build);

gulp.task['watch-elixir'] = gulp.task.watch;

gulp.task('watch', ['watch-elixir', 'watch-semantic']);

var elixir = require('laravel-elixir');
require('laravel-elixir-vue-2');

elixir(function (mix) {
    mix.copy('./node_modules/font-awesome/fonts/**', 'public/fonts');
    // calendar
    mix.copy('./assets/css/calendar.min.css', './public/css/calendar.min.css');
    mix.copy('./assets/js/calendar.min.js', './public/js/calendar.min.js');
    // semantic
    // mix.task('build-semantic'); // TODO: reanable this
    mix.copy('./assets/semantic/dist/semantic.min.css', './public/css/semantic.min.css');
    // mix.copy('./assets/semantic/src/themes', './public/css/themes');
    mix.copy('./assets/semantic/dist/semantic.min.js', './public/js/semantic.min.js');
    // custom
    mix.copy('./assets/img', './public/img');
    mix.copy('./assets/js/main.js', './public/js/main.js');
    mix.copy('./assets/favicon.ico', './public/favicon.ico');
    mix.sass('./assets/scss/app.scss', './public/css/app.css')
        .webpack('./app/app.js', './public/js/app.js');

    mix.browserSync({
        proxy: 'codefest.dev/2017-front/public/'
    });
});

(function ($) {
    $(function () {
        $('.right.menu.open').on("click", function (e) {
            e.preventDefault();
            $('.ui.vertical.menu').toggle();
        });

        $('#provincie-ontvanger').dropdown();
        $('#provincie-verstuurder').dropdown();
        $('#subcat').dropdown();
        $('#hoofdcat').dropdown();
        $('#example1').calendar({
            type: 'date',
            monthFirst: false
        });
        $('#example3').calendar({
            ampm: false,
            type: 'time'
        });

    });
})(jQuery);

// Load the Visualization API and the corechart package.
google.charts.load('current', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.charts.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and
// draws it.
function drawChart() {

    // Create the data table.
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Topping');
    data.addColumn('number', 'Slices');
    data.addRows([
        ['Onderweg', 3],
        ['Aangemeld', 3],
        ['Verzonden', 1]
    ]);

    // Set chart options
    var options = {'title':'Alle pakketen',
        'width':400,
        'height':300,
        pieSliceText: 'value'
    };

    // Instantiate and draw our chart, passing in some options.
    var el = document.getElementById('chart_div');
    if (el === null) return;
    var chart = new google.visualization.PieChart(el);
    chart.draw(data, options);
}
import auth from './services/auth'

export default [
    {
        path: '/',
        name: 'home',
        component: require('./components/home/home.vue')
    },
    {
        path: '/test',
        name: 'test',
        component: require('./components/test.vue')
    },

    {
        path: '/registreren',
        name: 'registreren',
        component: require('./components/auth/registreren.vue'),
        meta: {
            auth: false
        }
    },
    {
        path: '/login',
        name: 'login',
        component: require('./components/versturen/versturen.vue'),
        meta: {
            auth: false
        }
    },
    {
        path: '/logout',
        name: 'logout',
        component: require('./components/auth/logout.vue')
    },

    {
        path: '/traceer',
        name: 'traceer',
        component: require('./components/traceer/traceer.vue'),
        meta: {
            auth: false
        }
    },
    {
        path: '/verstuur',
        name: 'verstuur',
        component: require('./components/test.vue'),
        meta: {
            auth: false
        }
    },
    {
        path: '/ontvangen',
        name: 'ontvangen',
        component: require('./components/test.vue'),
        meta: {
            auth: false
        }
    },

    {
        path: '/contact',
        name: 'contact',
        component: require('./components/test.vue'),
        meta: {
            auth: false
        }
    },
    {
        path: '/klantenservice',
        name: 'klantenservice',
        component: require('./components/test.vue'),
        meta: {
            auth: false
        }
    },
    {
        path: '/sitemap',
        name: 'sitemap',
        component: require('./components/test.vue'),
        meta: {
            auth: false
        }
    },
    {
        path: '/persberichten',
        name: 'persberichten',
        component: require('./components/test.vue'),
        meta: {
            auth: false
        }
    },

    {
        path: '/gegevens',
        name: 'gegevens',
        component: require('./components/versturen/tabs/gegevens.vue')
    },
    {
        path: '/gegevens-gast',
        name: 'gegevens-gast',
        component: require('./components/test.vue')
    },
    {
        path: '/afrekenen',
        name: 'afrekenen',
        component: require('./components/versturen/tabs/afrekenen.vue')
    },
    {
        path: '/printen',
        name: 'printen',
        component: require('./components/versturen/tabs/printen.vue')
    }
]
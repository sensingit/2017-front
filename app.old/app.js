import {Vue, router, store} from './boot/core'
import App from './components/app.vue'

// services
import auth from './services/auth'

// Check the users auth status when the app starts
auth.checkAuth();

// TODO: check if vue-events and vue-stash are still necessary

// TODO: move this to it's own file (A more central location)
const DEBUG = true;
const DEBUG_KEY = 'XDEBUG_SESSION_START';
const DEBUG_VALUE = 'PHPSTORM';

Vue.http.interceptors.push(function (request, next) {
    // modify params
    if (DEBUG) request.params[DEBUG_KEY] = DEBUG_VALUE;

    // modify headers
    // request.headers.set('X-CSRF-TOKEN', 'TOKEN'); // TODO: add CSRF

    if (auth.identity.authenticated) { // TODO find a cleaner way to do this
        request.headers.set('Authorization', auth.getIdentity());
    }

    // continue to next interceptor
    next(function (response) {
        // modify response
        if (!response.ok) {
            if (!response.headers.has("Content-Type") && response.body === null) {
                console.warn('Ignore \'Access-Control-Allow-Origin\' header message.');
            }
            console.error('There is an error in the server response.');
        }
    });
});

new Vue({
    router,
    el: '#app',
    render: h => h(App),
    data: {store},
    http: {
        // root: '/root',
        headers: {
            'Access-Control-Allow-Origin': 'true',
        }
    }
});
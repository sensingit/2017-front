import {Router} from './vue'
import routes from './../routes'

const router = new Router({
    // mode: 'history', // uncomment to using html5 history features.
    // hashbang: false, // uncomment to remove the hashbang from the url
    linkActiveClass: 'active',
    routes
});

// authentication service
import auth from '../services/auth';

/*router.beforeEach((to, from, next) => {
    if ((to.meta.auth || to.meta.auth == undefined) && !auth.identity.authenticated) {
        let query = {};
        // add requested route as query param
        // if (to.name !== 'logout') query.redirect = to.name; //TODO: implement the other side of this
        // if route requires auth and user isn't authenticated
        next({
            name: 'login',
            query: query
        });
        return;
    }
    next();
});*/

export default router;

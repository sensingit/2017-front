import auth from './auth'

// endpoints
const DEBUG = true;
const DEBUG_KEY = 'XDEBUG_SESSION_START';
const DEBUG_VALUE = 'PHPSTORM';

const HOST = 'http://backend.vue.dev'; //http://auth.bigparcel.nl/';
const ORDER_URI = HOST + '/v1/order'; //;
const INPUT_NAME_PICKUP_START_DATE = "order[pickup_start][date]";
const INPUT_NAME_PICKUP_START_TIME = "order[pickup_start][time]";
const INPUT_NAME_PICKUP_END_DATE = "order[pickup_end][date]";
const INPUT_NAME_PICKUP_END_TIME = "order[pickup_end][time]";
const INPUT_NAME_PICKUP_START = "order[pickup_start]";
const INPUT_NAME_PICKUP_END = "order[pickup_end]";
const TIMEZONE = "+01:00";
const IDENTITY = 1;

// TODO: add method argument to sendForm
export default {
    // identity object is how we check auth status

    sendForm(context, data, callback){
        let self = this;

        this.addDateTime(data, INPUT_NAME_PICKUP_START_DATE, INPUT_NAME_PICKUP_START_TIME, INPUT_NAME_PICKUP_START);
        this.addDateTime(data, INPUT_NAME_PICKUP_END_DATE, INPUT_NAME_PICKUP_END_TIME, INPUT_NAME_PICKUP_END);

        console.log(data.get("identity"));

        context.$http.post(ORDER_URI, data).then(
            (response) => {
                if (!response.hasOwnProperty('body')) {
                    throw Error('Expected object with property "body".');
                }
                let bodyObject = self.parseJWT(response.body);
                console.log(bodyObject);

                self.identity.authenticated = true;

                // callback
                if (callback) {
                    callback(self);
                }
            },
            (err) => {
                context.error = err
            }
        )
    },
    createDateTime(date, time) {
        //date = new Date(date + "T" + time + TIMEZONE);
        date = new Date(date + " " + time);
        if (Object.prototype.toString.call(date) === "[object Date]") {
            // it is a date
            console.log("is date");
            if (isNaN(date.getTime())) {  // d.valueOf() could also work
                // date is not valid
                console.log("date not valid");
            }
            else {
                console.log("date valid");
                // date is valid
            }
        }
        else {
            console.log("Not valid datetime");
            // not a date
        }

        return date.toJSON();
    },
    addDateTime(data, inputNameDate, inputNameTime, inputNameKey) {
        let date = data.get(inputNameDate);
        let time = data.get(inputNameTime);
        data.delete(inputNameDate);
        data.delete(inputNameTime);
        data.append(inputNameKey, this.createDateTime(date, time));
    },

    trace(context, data, callback, errorHandler){
        let self = this;

        let uri = ORDER_URI;

        auth.getIdentity(context.$http.headers.common);

        data = Array.from(data.entries()).reduce(function (acc, p) {
            acc[p[0]] = p[1];
            return acc;
        }, {});

        /*
         serialize = function(obj) {
         var str = [];
         for(var p in obj)
         if (obj.hasOwnProperty(p)) {
         str.push(encodeURIComponent(p) + encodeURIComponent(":") + encodeURIComponent(obj[p]));
         }
         return str.join(encodeURIComponent(";"));
         }
         // OR
         encodeURIComponent(JSON.stringify({foo: "hi there", bar: "100%" }))
        */

        // console.log(data);
        context.$http.get(uri, {
            params: {
                criteria: data
            }
        }).then(
            (response) => {
                if (!response.hasOwnProperty('body')) {
                    throw Error('Expected object with property "body".');
                }
                let bodyObject = response.body;
                console.log(bodyObject);
                console.log(response.headers);
                console.log(response.headers.has('Authorization'));
                auth.setIdentity(response.headers.get('Authorization')); // TODO: move to interceptor

                // callback
                if (callback) {
                    callback(self);
                }
            },
            (err) => {
                let body = err.body;
                context.error = body.hasOwnProperty('error') ? body.error : body;
                errorHandler(err);
            }
        )
    }
}